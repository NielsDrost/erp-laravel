<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('boms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('item_id_parent');
			$table->integer('item_id_child')->nullable();
			$table->string('status');
			$table->integer('user_id');
			$table->integer('type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('boms');
	}

}
